@extends('layouts.myapp')

@section ('content')
<!--=================== content body ====================-->
<div class="col-lg-10 col-md-9 col-12 body_block  align-content-center">
    <div class="blog">
        <div class="row justify-content-center">
            <div class="col-lg-4 col-md-12 col-12">
              @foreach ($images as $image)
                <img src='/assets/img/{{ $image->src }}' alt='{{ $image->alt }}' />
              @endforeach
            </div>
            <div class="col-lg-8 col-md-12 col-12">

                <article>
                	@isset ($project['name'])

                		<h1>{{ $project['name'] }}</h1>
                	@endisset
                  @isset ($project['description'])
                    <p>{{ $project['description'] }}</p>
                  @endisset

                </article>
            </div>
        </div>
    </div>
</div>
<!--=================== content body end ====================-->
@endsection
