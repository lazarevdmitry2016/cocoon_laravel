@extends('layouts.myapp')

@section ('content')
<!--=================== content body ====================-->
<div class="col-lg-10 col-md-9 col-12 body_block  align-content-center">
    <div>
        <div class="img_card">
            <div class="row justify-content-center">
                <div class="col-md-6 col-7 content_section">
                    <div class="content_box">
                        <div class="content_box_inner">
                            <div>
                                @isset($info['about_me_title'])
                                <h3>
                                {{ $info['about_me_title'] }}
                                </h3>
                                @endisset
                                @isset($info['about_me_text'])
                                <p>
                                    {{ $info['about_me_text'] }}
                                </p>
                                @endisset
                                @isset ($counters)
                                <!--=================== counter start====================-->
                                <div class="pt50">
                                    <div class="row justify-content-center">

                                        @foreach ($counters as $counter)
                                        <div class="col-12 col-md-4">
                                            <div class="counter_box">
                                                <div class="divider"></div>
                                                <span class="counter">{{ $counter->number }}</span>
                                                <h5>{{ $counter->text }}</h5>
                                            </div>
                                        </div>
                                        @endforeach

                                    </div>
                                </div>
                                <!--=================== counter end====================-->
                                @endisset
                            </div>
                            @isset ($testimonials)
                            <!--===================testimonial start====================-->
                            <div class="testimonial_carousel mt50">

                                @foreach ($testimonials as $t)
                                <div class="testimonial_box">
                                    <p>
                                      {{ $t['text'] }}
                                    </p>
                                    <div class="testimonial_author">
                                        <!-- <img src="" alt="{{ $t['author'] }}"> -->
                                        <h5>{{ $t['author'] }}</h5>
                                        <!-- <p>project manager <span>company</span></p> -->
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <!--===================testimonial end====================-->
                            @endisset
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-5 img_section" style="background-image: url('{{ $info["image"]["url"] }}');"></div>
            </div>
        </div>
    </div>
</div>
<!--=================== content body end ====================-->
@endsection
