@extends('layouts.myapp')

@section ('content')
<!--=================== content body ====================-->
<div class="col-lg-10 col-md-9 col-12 body_block  align-content-center">
    <div class="blog">
        <div class="row justify-content-center">
            <div class="col-lg-4 col-md-12 col-12">
                <div class="sidebar">
                  <!--
                    <div class="widget widget_search">
                        <div class="search-form">
                            <input type="text" class="search-field" placeholder="Search">
                        </div>
                    </div>
                  -->
                    <div class="widget widget_categories">
                    @if (isset($categories) && count($categories)>0)
                        <h4 class="widget-title">
                            Categories
                        </h4>
                        <ul>
                        @foreach ($categories as $category)
                            <li><a href="/blog/category/{{ $category['id'] }}">{{ $category['name'] }}</a></li>
                        @endforeach
                        </ul>
                    @endif
                    </div>
                    <div class="widget widget_instagram">
                    @isset ($social['instagram'])
                        <h4 class="widget-title">
                            Instagram
                        </h4>
                        <ul>
                        @isset ($social['instagram']['photos'])
                          @foreach ($social['instagram']['photos'] as $photo)
                              <li>
                                <a href="{{ $photo['href'] }}">
                                  <img src="/assets/img/{{ $photo['src'] }}" alt="{{ $photo['alt'] }}">
                                </a>
                              </li>
                          @endforeach
                        @endisset
                        </ul>
                    @endisset
                    </div>
                    <div class="widget widget_tags">
                    @if (count($tags)>0)
                        <h4 class="widget-title">
                            Tags
                        </h4>
                        <ul>
                        @foreach ($tags as $tag)
                            <li>
                              <a href="/blog/tag/{{ $tag['name'] }}">{{ $tag['name'] }}</a>
                            </li>
                        @endforeach
                        </ul>
                    @endif
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-12 col-12">
              @if (count($posts)>0)
                @foreach ($posts as $post )


                <article class="blog_card">
                    <div class="blog_card_top">
			@isset($post->image)
			<img src="{{ $post->image->src }}" alt="" />
			@endisset
                        @isset($post->date)
                        <div class="blog_date">
			    @isset($post->date_formatted)
                            {{ $post->date_formatted['day'] }}
                            <span>
                                {{ $post->date_formatted['month'] }}  {{ $post->date_formatted['year'] }}
                            </span>
                            @else
			    {{ $post->date }}
			    @endisset
			    

                        </div>
                        @endisset
                    </div>
                    <div class="blog_card_bottom">
                        <h4>
                          @isset ($post->href)
                            <a href="/posts/{{ $post->permalink }}">
                                {{ $post->name }}
                            </a>
                          @else
                          {{ $post->name }}
                          @endisset
                        </h4>
                        <div class="meta_data">
			    @isset($post->author)
                            <span>By {{ $post->author['name'] }}</span>
			    @endisset
                            <span>
				@isset($post->date['day'])
				{{ $post->date['day'] }}  {{ $post->date['month'] }}  {{ $post->date['year'] }}
                        	      @else
				{{ $post->date }}
				@endisset
                            </span>
			    @if($post->comments != null)
				<span>{{ $post->comments->count()  }} Comments</span>
			    @else
				<span>0</span>
			    @endif
			    
                        </div>
                        <p>
                            {{ $post->brief_content }}
                        </p>
                    </div>
                </article>
                @endforeach
	      @else
		NO posts
              @endif
            </div>
        </div>
    </div>
</div>
<!--=================== content body end ====================-->
@endsection
