<!DOCTYPE html>
<html lang="en">
<head>
    <!-- ========== Meta Tags ========== -->
    <meta charset="UTF-8">

    <meta name="description" content="{{ $description??'' }}">
    <meta name="keywords" content="{{ $keywords??'' }}">

    <meta name="author" content="Pharaohlab">
    <meta name="author" content="Dmitry Lazarev (https://bitbucket.org/lazarevdmitry2016/cocoon_laravel/src/master/)">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=6">
    <!-- ========== Title ========== -->
    <title> Cocoon -Portfolio</title>
    <!-- ========== Favicon Ico ========== -->
    <link rel="icon" href="/assets/img/logo.png">
    <link rel="preconnect" href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,500&display=swap" >
    <!-- ========== STYLESHEETS ========== -->
    <!-- Bootstrap CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Fonts Icon CSS -->
    <link href="/assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="/assets/css/et-line.css" rel="stylesheet">
    <link href="/assets/css/ionicons.min.css" rel="stylesheet">
    <!-- Carousel CSS -->
    <link href="/assets/css/slick.css" rel="stylesheet">
    <!-- Magnific-popup -->
    <link rel="stylesheet" href="/assets/css/magnific-popup.css">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="/assets/css/animate.min.css">
    <!-- Custom styles for this template -->
    <link href="/assets/css/main.css" rel="stylesheet">
</head>
<body>
<div class="loader">
    <div class="loader-outter"></div>
    <div class="loader-inner"></div>
</div>

<div class="body-container container-fluid">
    <a class="menu-btn" href="javascript:void(0)">
        <i class="ion ion-grid"></i>
    </a>
    <div class="row justify-content-center">
        <!--=================== side menu ====================-->
        <div class="col-lg-2 col-md-3 col-12 menu_block">

            <!--logo -->
            <div class="logo_box">
                <a href="#">
                    <img src="/assets/img/logo.png" alt="cocoon">
                </a>
            </div>
            <!--logo end-->

            <!--main menu -->
            <div class="side_menu_section">
                <ul class="menu_nav">
		 @isset($page)
                    @if ($page == 'home')
                    <li class="active">
                    @else
                    <li>
                    @endif
                        <a href="{{ route('home') }}">
                            Home
                        </a>
                    </li>
                    @if ($page == 'about')
                    <li class="active">
                    @else
                    <li>
                    @endif
                        <a href="{{ route('about') }}">
                            About Us
                        </a>
                    </li>
                    @if ($page == 'services')
                    <li class="active">
                    @else
                    <li>
                    @endif
                        <a href="{{ route('services') }}">
                            Services
                        </a>
                    </li>
                    @if ($page == 'portfolio')
                    <li class="active">
                    @else
                    <li>
                    @endif
                        <a href="{{ route('portfolio') }}">
                            Portfolio
                        </a>
                    </li>
                    @if ($page == 'blog')
                    <li class="active">
                    @else
                    <li>
                    @endif
                        <a href="{{ route('blog') }}">
                            Blog
                        </a>
                    </li>
                    @if ($page == 'contact')
                    <li class="active">
                    @else
                    <li>
                    @endif
                        <a href="{{ route('contact') }}">
                            Contact
                        </a>
                    </li>
		    @endisset
                </ul>
            </div>
            <!--main menu end -->

            <!-- here will be filters -->

            <!--social and copyright -->
            <div class="side_menu_bottom">
                <div class="side_menu_bottom_inner">
                    <ul class="social_menu">
                        <li>
                            <a href="https://bitbucket.org/lazarevdmitry2016/cocoon_laravel/src/master/" target="_blank" rel="noopener"> <i class="fa fa-bitbucket"></i> </a>
                        </li>
                        <li>
                            <a href="https://lazarev-dmitry.ru"  target="_blank" rel="noopener"> <i class="fa fa-link"></i> </a>
                        </li>
                    </ul>
                    <div class="copy_right">
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        <p class="copyright">Copyright &copy;2020 All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" rel="noopener">Colorlib</a></p>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </div>
                </div>
            </div>
            <!--social and copyright end -->

        </div>
        <!--=================== side menu end====================-->

        @yield('content')
    </div>
</div>


<!-- jquery -->
<script src="/assets/js/jquery.min.js"></script>
<!-- bootstrap -->
<script src="/assets/js/popper.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/waypoints.min.js"></script>
<!--slick carousel -->
<script src="/assets/js/slick.min.js"></script>
<!--Portfolio Filter-->
<script src="/assets/js/imgloaded.js"></script>
<script src="/assets/js/isotope.js"></script>
<!-- Magnific-popup -->
<script src="/assets/js/jquery.magnific-popup.min.js"></script>
<!--Counter-->
<script src="/assets/js/jquery.counterup.min.js"></script>
<!-- WOW JS -->
<script src="/assets/js/wow.min.js"></script>
<!-- Custom js -->
<script src="/assets/js/main.js"></script>
</body>
</html>
