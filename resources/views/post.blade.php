@extends('layouts.myapp')

@section ('content')
<!--=================== content body ====================-->
<div class="col-lg-10 col-md-9 col-12 body_block  align-content-center">
    <div class="blog">
        <div class="row justify-content-center">

            <div class="col-lg-12 col-md-12 col-12">
                <article class="blog_card">
                    <div class="blog_card_top">

			@isset($image)
			    <img style="width:50%;float:left;margin-right:1.5em;margin-bottom:1.5.em" src="{{ $image['src'] }}" alt="{{ $image['alt'] }}" />
			@endisset
                        @isset($images)
			@foreach($images as $image)

			    @isset($image['src'])
                            <img style="width:50%;float:left;margin-right:1.5em;margin-bottom:1.5.em"
				 src="{{ $image['src'] }}" alt="{{ $image['alt'] }}" />
			    @else
			    @endisset
			@endforeach

			@else
			no images
                        @endisset
                        <div class="blog_date">
			    @isset($date['day'])
                            {{ $date['day'] }}
                            <span>
                                {{ $date['month'] }}{{ $date['year'] }}
                            </span>
			    @else
			    <span>{{ $date }}</span>
			    @endisset

                        </div>
                    </div>
                    <div class="blog_card_bottom">
                        <h4>
                            <a href="#">
                                {{ $name }}
                            </a>
                        </h4>
                        <div class="meta_data">
			    @isset($authors)
			        @foreach($authors as $author)
				    <span>By {{ $author->name }}</span>
				@endforeach
				
			    @else
			    @endisset
                            @isset ($comments)
                            <span>{{ count($comments) }} Comments</span>
                            @endisset
                        </div>
                        <p>
                            {{ $content }}
                        </p>
                    </div>
                </article>
                <article>
                @isset ($comments)
                    <h5>Comments: </h5>
                    @foreach ($comments as $comment)
                    <div class="row">
                        <div class="col-lg-2 "></div>
                        <div class="col-lg-10">
                            <p>{{ $comment['text'] }}</p>
                        </div>
                        <div class="col-lg-11">
                           {{--  <h6 style="text-align:right;">{{ $comment['author'] }}</h6>  --}}
                        </div>
                    </div>
                    @endforeach
                @else
                    <p>No Comments Yet.</p>
                @endisset
                </article>
            </div>
        </div>
    </div>
</div>
<!--=================== content body end ====================-->
@endsection
