
<!--main menu -->
<div class="side_menu_section" dusk="side_menu_section">
    <ul class="menu_nav">

        <li> <!-- class="active">-->
            <a href="/">
                Home
            </a>
        </li>
        <li>
            <a href="/about">
                About Us
            </a>
        </li>
        <li>
            <a href="/services">
                Services
            </a>
        </li>
        <li>
            <a href="/portfolio">
                Portfolio
            </a>
        </li>
        <li>
            <a href="/blog">
                Blog
            </a>
        </li>
        <li>
            <a href="/contact">
                Contact
            </a>
        </li>
    </ul>
</div>
<!--main menu end -->
