@extends('layouts.myapp')

@section ('content')
<!--=================== content body ====================-->
<div class="col-lg-10 col-md-9 col-12 body_block  align-content-center">
    <div>
        <div class="img_card">
            <div class="row justify-content-center">
                <div class="col-md-6 col-7 content_section">
                    <!--=================== contact info and form start====================-->
                    <div class="content_box">
                        <div class="content_box_inner">
                            <div>
                                <h3>
                                {{ $info['contact']['title'] }}
                                </h3>
                                <p>
                                    {{ $info['contact']['text'] }}
                                </p>

                                <ul class="nosyely_list pt50">
                                	@isset ($info['address'])
                                    <li>
                                        <div class="img_box_two">

                                            <img src="assets/img/icons/satelite.png" alt="info list">

                                            <div class="content align-items-center">
                                                <p>
                                                    Address: {{ $info['address'] }}<br/>
                                                </p>
                                            </div>

                                        </div>

                                    </li>
                                    @endisset
                                    @isset ($info['email'])
                                    <li>
                                        <div class="img_box_two">
                                            <img src="assets/img/icons/scheme.png" alt="info list">
                                            <div class="content align-items-center">
                                                <p>
                                                    E-mail: {{ $info['email'] }}
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    @endisset
                                    @isset ($info['phonenumber'])
                                    <li>
                                        <div class="img_box_two">
                                            <img src="assets/img/icons/smartphone.png" alt="info list">
                                            <div class="content align-items-center">
                                                <p>
                                                    Phonenumber: {{ $info['phonenumber'] }}
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    @endisset
                                </ul>
                                <form method="post" action="/contact">
                                  {{ csrf_field() }}
                                  @if ($errors->any())
                                    @foreach ($errors->all() as $error)
                                      <div class="row">
                                        {{ $error }}
                                      </div>
                                    @endforeach
                                  @endif
                                  <div class="mt75 row justify-content-center">
                                      <div class="col-lg-6 col-12">
                                          <input type="text" placeholder="Ваше имя" class="form-control" name="name">
                                      </div>
                                      <div class="col-lg-6 col-12">
                                          <input type="email" placeholder="E-Mail" class="form-control" name="email">
                                      </div>
                                      <div class="col-12">
                                          <input type="text" placeholder="Тема сообщения" class="form-control" name="text">
                                      </div>
                                      <div class="col-12">
                                          <textarea  placeholder="Сообщение" class="form-control" cols="4" rows="4" name="message"></textarea>
                                      </div>
                                      <div class="col-12">
                                          <button type="submit" class="btn btn-primary">Отправить</button>
                                      </div>
                                  </div>
                              </form>
                            </div>
                        </div>
                    </div>
                    <!--=================== contact info and form end====================-->
                </div>
                <div class="col-md-6 col-5 img_section" style="background-color:#000;">
                    <!--map -->
                    <div id="map" data-lon="24.036176" data-lat=" 57.039986" class="map"></div>
                    <!--map end-->
                </div>
            </div>
        </div>
    </div>
</div>
<!--=================== content body end ====================-->
@endsection
