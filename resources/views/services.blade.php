@extends('layouts.myapp')

@section ('content')
<!--=================== content body ====================-->
<div class="col-lg-10 col-md-9 col-12 body_block  align-content-center">
    <div>
      <!--=================== image card start here  (has two section left and right )====================-->
        <div class="img_card">
            <div class="row justify-content-center">
                <div class="col-md-6 col-7 content_section">
                    <div class="content_box">
                        <div class="content_box_inner">
                            <div class="row justify-content-center">
                            @if (count($services)>0)
                              <!--=================== services boxes start here  ====================-->
                                @foreach ($services as $service)
                                <div class="col-md-6 col-12">
                                    <div class="img_box_one text-left">

					
					@isset($service->images)
					@foreach($service->images as $image)
					<img src="{{ $image->src }}" alt="{{ $image->alt  }}">
					@endforeach
					@endisset
                                        <div class="content">
                                            <h5>
                                                {{ $service['name'] }}
                                            </h5>
                                            <p>
                                                {{ $service['text'] }}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                <!--=================== services boxes end here  ====================-->
                            @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-5 img_section" style="background-image: url('assets/img/bg/service_bg.png');"></div>
            </div>
        </div>
        <!--=================== image card end here ====================-->
    </div>
</div>
<!--=================== content body end ====================-->
@endsection
