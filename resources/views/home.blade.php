@extends('layouts.myapp')

@section ('content')
<!--=================== content body ====================-->
<div class="col-lg-10 col-md-9 col-12 body_block  align-content-center">
    <div class="portfolio">
        <div class="container-fluid">
            <!--=================== masaonry portfolio start====================-->
            <div class="grid img-container justify-content-center no-gutters">
                <div class="grid-sizer col-sm-12 col-md-6 col-lg-3"></div>
                @foreach ($projects as $project)
                    @if ($loop->iteration == 1)
                <div class="grid-item branding  col-sm-12 col-md-6 col-lg-3">
                    @elseif($loop->iteration == 2)
                <div class="grid-item  branding architecture  col-sm-12 col-md-6">
                    @elseif($loop->iteration == 3)
                <div class="grid-item  design col-sm-12 col-md-6 col-lg-3">
                    @elseif($loop->iteration == 4)
                <div class="grid-item  photography design col-sm-12 col-md-6 col-lg-3">
                    @elseif($loop->iteration == 5)
                <div class="grid-item  branding photography  col-sm-12 col-md-6 col-lg-3">
                    @elseif($loop->iteration == 6)
                <div class="grid-item   architecture design col-sm-12 col-md-6 col-lg-3">
                    @elseif($loop->iteration == 7)
                <div class="grid-item  photography architecture col-sm-12 col-md-6 col-lg-3">
                    @elseif($loop->iteration == 8)
                <div class="grid-item  branding design  col-sm-12 col-md-6 col-lg-3">
                    @elseif($loop->iteration == 9)
                <div class="grid-item architecture  col-sm-12 col-md-6 col-lg-6">
                    @endif
                    <a href="/portfolio/projects/{{ $project['href'] }}" title="{{ $project['name'] }}">
                        <div class="project_box_one">
                            @isset ($project['image'])
                            <img src="/assets/img/{{ $project['image']['src'] }}" alt="{{ $project['image']['alt'] }}" />
                            @else
                            No image
                            @endisset
                            <div class="product_info">
                                <div class="product_info_text">
                                    <div class="product_info_text_inner">
                                        <i class="ion ion-plus"></i>
                                        <h4>{{ $project['name'] }}</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
            <!--=================== masaonry portfolio end====================-->
        </div>
    </div>
</div>

<!--=================== content body end ====================-->
@endsection
