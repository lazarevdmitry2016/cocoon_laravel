<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Information;
use App\Models\Counter;
use App\Models\Testimonial;

class About extends Controller
{
    private static $page="about";
    //
    public function index()
    {
        $testimonials=Testimonial::all();

        $data=array(
            'info'=>array(
                'image'=>array(
                'url'=>''
                ),
                'about_me_title'=>Information::getValue('about_me_title'),
                'about_me_text'=>Information::getValue('about_me_text'),
            ),
            'counters'=>Counter::all(),
        );

        if (count($testimonials) > 0){
            $data['testimonials']=$testimonials;
        }
        About::addInfo($data,self::$page);
        // dd($data);
        return view('about',$data);
    }
}
