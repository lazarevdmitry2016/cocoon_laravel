<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Author;
use App\Models\Category;
use App\Models\Image;

use App\Models\Social;
use App\Models\Tag;
use App\Models\Post;
use App\Models\ImagesForPost;

class Blog extends Controller
{
    private static $page="blog";

    //

    public function index()
    {
        $posts = Post::with(['image','images','comments','authors'])->get();
        $data = array(
            'posts' => $posts,
            'tags' => Tag::all(),
            'categories' => Category::all()
        );
        self::addInfo($data,'blog');
        return view('blog',$data);
    }

    public function getPostsByTag($tag)
    {
        $posts = [];
        if (Tag::where('name',$tag)->count() > 0)
        {
            $temp_posts=Tag::where('name',$tag)->first()->posts;// items from Tags_for_post table (array)
            $ids = [];
            foreach ($temp_posts as $temp_post)
            {
                $ids[] = $temp_post->id;
            }
            $posts = Post::with(
                [
                    'images',
                    'comments',
                    'tags',
                    'categories',
                    'authors'
                ]
            )->whereIn('id',$ids)->get();
        } 
        $data=[
            'categories'=>Category::all(),
            'social'=>null,
            'tags'=>Tag::all(),
            'posts'=>$posts
        ];
        self::addInfo($data,'blog');
        return view('blog',$data);
    }

    public function getPostsByCategoryId($category_id)
    {
        $posts = Category::findOrFail($category_id)->posts;

        $data=[
            'categories'=>Category::all(),
            'social'=> null,
            'tags'=>Tag::all(),
            'posts'=> $posts
        ];
        self::addInfo($data,'blog');
        return view('blog',$data);
    }

    public function getPostBySlug($slug=null)
    {
        if (is_null($slug)){
            // show 404 page
            return redirect()->route('/',304);
        } else {
            $posts=Post::with(['image','images','comments','tags','categories','authors'])->where('permalink',$slug);

            if ($posts->count() > 0){
                $post=$posts->first();
                self::addInfo($post,'post');
                return view('post',$post);
            } else {
                return redirect()->route('home');
            }
        }
    }

    public function getPostById($id)
    {
        $post=Post::with(['image','images'])->findOrFail($id); //->first();
        self::addInfo($post,'post');
        return view('post',$post);
    }

}
