<?php

namespace App\Http\Controllers;
use App\Models\Information;
use App\Models\Submission;
use Illuminate\Http\Request;
use App\Models\Http\Requests\SubmissionRequest;


class Contact extends Controller
{
    private static $page="contact";
    //

    public function index()
    {
        $data=array(
            'info'=>array(
                'contact'=>array(
                'title'=>Information::getValue('contact_title'),
                'text'=>Information::getValue('contact_text')
                ),
                'email'=>Information::getValue('contact_email'),
                'phonenumber'=>Information::getValue('contact_phonenumber'),
                'address'=>Information::getValue('contact_address')
            )
        );
        Contact::addInfo($data,self::$page);
        return view('contact',$data);
    }

    public function createSubmission(SubmissionRequest $request)
    {

        Submission::create($request->input());
        return redirect()->route('contact');
    }
}
