<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\Information;
use App\Models\Image;

class Home extends Controller
{
    private static $page="home";
    //

    public function index()
    {
        $projects=Project::all();
        $p=[];
        if (isset($projects) && count($projects) >0){
            foreach($projects as $project){
                if (isset($project->images_for_project->first()->image)){
                    $image=$project->images_for_project->first()->image;
                    $p[]=array(
                        "image"=>array(
                            "src"=>$image->src,
                            "alt"=>$image->alt
                        ),
                        "name"=>$project->name,
                        "href"=>$project->href,
                    );
                } else {
                $image=Image::where('alt','blank')->first();

                $p[]=array(
                    "image"=>array(
                        "src"=>$image->src,
                        "alt"=>$image->alt
                    ),
                    "name"=>$project->name,
                    "href"=>$project->href,
                );
                }


            }
        }
        $data=array(
            "projects"=>$p,
            "filters"=>array()
        );
        Home::addInfo($data,self::$page);

        return view('home',$data);
    }
}
