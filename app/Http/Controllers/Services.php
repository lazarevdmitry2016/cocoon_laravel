<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Service;

class Services extends Controller
{
    public static $page="services";
    //
    public function index()
    {
        $data=array(
            "services"=>Service::with(['images'])->get(),
        );
        Services::addInfo($data,self::$page);
        return view('services',$data);
    }
}
