<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project as ProjectModel;
use App\Models\ImagesForProject;

class Project extends Controller
{
    public static $page="project";
    //
    public function index($projectHref)
    {
        $data=array();
            if (isset($projectHref)){
            $project=ProjectModel::where('href',$projectHref)->first();
            $data['project']=$project;
            $data['images']=$project->images_for_project->image;

            Project::addInfo($data,self::$page);
            $data['description']=$project['description'];
            $data['keywords']=$project['keywords'];

            return view('project',$data);
            } else {
                // return 404 page
            }
    }

}
