<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use App\Models\Information;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public static function addInfo(&$data,$page)
    {
        $data["page"]=$page;
        $data["description"]=Information::getValue('description',$page);
        $data["keywords"]=Information::getValue('keywords',$page);
        $data["author"]=Information::getValue('author');
    }

    /*

    */
    protected static function addSelect($args=null)
    {
        if (!is_null($args)){
            $model_class=$args['model_class'];
            $model_field=$args['model_field'];
            $name=$args['name'];
            $title=$args['title'];
            $form=$args['form'];
            $host_class=$args['host_class'];
            $id=$args['id'];

            $items=$model_class::all();
            $options=[];

            foreach ($items as $item)
            {
                $options[$item->id]=$item->$model_field;
            }
            if (!is_null($id)){
                // gets selected one
                $selected_item=$model_class::find(
                    $host_class::find($id)->first()->author_id
                )->first();

                // inserts selected item as the first one in $options array
                $options[0]=$selected_item->name;
            }

            $form->select($name.'_id',$title)->options($options);
        }
    }

    public static function addMultipleSelect($args=null)
    {
        if (!is_null($args)){
            $model_class=$args['model_class'];
            $model_field=$args['model_field'];
            $name=$args['name'];
            $title=$args['title'];
            $form=$args['form'];

            $items=$model_class::all();
            $options=[];
            foreach ($items as $item)
            {
                $options[$item->id]=$item->$model_field;
            }
            $form->multipleSelect($name.'_id',$title)->options($options);
        }
    }

    /**/
    public static function storeAdminField($args=null)
    {
        if (!is_null($args)){
            $request=$args['request'];
            $c_name=$args['c_name']; // corresponding name
            $name=$args['name'];
            $id=$args['id'];

            $items=$request->input($c_name.'_id');// !!!
            // dd($request);
            $pivot_model_class=$args['pivot_model_class'];

            $pivot_model_class::where($name.'_id',$id)->delete();
            if (is_string($items)){
                $pivot_model_class::create([
                    $c_name.'_id'=>$items,
                    $name.'_id'=>$id
                ]);
            } else {
                foreach ($items as $item)
                {
                    if (!is_null($item)){
                        $pivot_model_class::create([
                            $c_name.'_id'=>$item,
                            $name.'_id'=>$id
                        ]);
                    }
                }
            }


        }
    }

    /*    storeAdminActioni) method
    *    @param    $params    an array of variables
    *    @return
    */
    public static function storeAdminAction($params)
    {
        $request=$params['request'];
        $model_class=$params['model_class'];
        $id=$params['id'];

        if ($request->method() == 'PUT')
        {
            $model_class::find($id)->update(
                $request->input()
            );
        }
    }
    /*
    *     createAdminAction() method
    *    @param    $params    an array of variables
    *    @return
    */
    public static function createAdminAction($params)
    {
        $request=$params['request'];
        $model_class=$params['model_class'];
        $redirect_route=$params['redirect_route'];
        $content=$params['content'];
        $form=$params['form'];

       if ($request->method() == 'POST')
        {
            $model_class::create(
                $request->input()
            );
            return redirect(route($redirect_route));
        }
        return $content
            ->header('Create')
            ->description('description')
            ->body($form);
    }
    /*
    *     deleteAdminAction() method
    *    @param    $params
    *    @return
    */
    public static function deleteAdminAction($params)
    {
        $model_class=$params['model_class'];
        $id=$params['id'];
        $redirect_route=$params['redirect_route'];

        $model_class::findOrFail($id)->delete();
        return redirect(route($redirect_route));
    }
}
