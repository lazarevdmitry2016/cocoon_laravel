<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TagsForPost extends Model
{
    //
    protected $fillable=[
        'post_id','tag_id'
    ];

    public function tag()
    {
        return $this->belongsTo('App\Models\Tag');
    }

    public function post()
    {
        return $this->belongsTo('App\Models\Post');
    }
}
