<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImagesForService extends Model
{
    //
    public function image()
    {
        return $this->belongsTo('App\Models\Image');
    }
    public function service()
    {
        return $this->belongsTo('App\Models\Service');
    }
}
