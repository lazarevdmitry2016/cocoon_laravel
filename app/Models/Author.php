<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Author extends Model
{
    //
    protected $fillable=[
        'name'
    ];

    public function post()
    {
        return $this->hasMany('App\Models\Post');
    }
}
