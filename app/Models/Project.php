<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\ImagesForProject;

class Project extends Model
{
  //
  protected $fillable=[
    'created_at','updated_at','href','name','description'
  ];

  public function images_for_project()
  {
    return $this->hasMany('App\Models\ImagesForProject');
  }

  // static methods

  /*
    Method getAllProjectsFormatted()

    @return	array of projects or an empty array
  */
    public static function getAllProjectsFormatted()
    {
        $data=array();
        $projects=Project::all();
        if (isset($projects) && count($projects) >0){
        foreach($projects as $project)
        {
            $image=$project->images_for_project->image;
            if (isset($image)){
            $data[]=array(
                "image"=>array(
                    "src"=>$image->src,
                    "alt"=>$image->alt
                ),
                "name"=>$project->name,
                "href"=>$project->href,
            );
                }
            }
        }
        return $data;
     }

}
