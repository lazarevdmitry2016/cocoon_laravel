<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    //
    protected $fillable=[
        'name','href'
    ];

    public function images_for_social()
    {
        return $this->hasMany('App\Models\ImagesForSocial');
    }

    // static methods


}
