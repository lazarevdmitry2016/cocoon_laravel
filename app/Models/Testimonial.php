<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    //
    protected $fillable=[
        'created_at','updated_at','text','author_id'
    ];
}
