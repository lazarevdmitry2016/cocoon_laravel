<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Image;
use Illuminate\Support\Facades\DB;

class ImagesForProject extends Model
{

    protected $fillable=[
        'created_at','updated_at','image_id','project_id'
    ];
	public function image()
	{
		return $this->belongsTo('App\Models\Image');
	}
	public function project()
	{
		return $this->belongsTo('App\Models\Project');
	}


}
