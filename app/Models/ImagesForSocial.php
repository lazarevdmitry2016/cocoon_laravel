<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImagesForSocial extends Model
{
    //
    protected $fillable=[
        'social_id','image_id'
    ];
    public function image()
    {
        return $this->belongsTo('App\Models\Image');
    }
    public function social()
    {
        return $this->belongsTo('App\Models\Social');
    }
}
