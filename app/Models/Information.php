<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Information extends Model
{
    protected $fillable=[
        'value'
    ];
    /*
        Method getValue()

        @param  $key    string  name of a key
        @param  $page_name  string  optional    page's name

        @return string  value corresponding to the $key
    */
    public static function getValue($key,$page_name=null)
    {
        if (is_null($page_name)){
            $value=self::where('key',$key);
        } else {
            $value=self::where('key',$page_name."_".$key);
        }
        return ($value->count() > 0)?$value->first()->value:'';
    }
}
