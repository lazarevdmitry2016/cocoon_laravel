<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //
    protected $fillable=[
        'created_at','updated_at','href','name'
    ];

    public function posts()
    {
        return $this->belongsToMany('App\Models\Post','tags_for_posts','post_id','tag_id');
    }
}
