<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImagesForPost extends Model
{
    //
    public function image()
    {
        return $this->belongsTo('App\Models\Image');
    }

    public function post()
    {
        return $this->belongsTo('App\Models\Post');
    }

}
