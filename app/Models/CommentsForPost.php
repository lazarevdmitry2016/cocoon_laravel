<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommentsForPost extends Model
{
    //
    protected $fillable=[
        'comment_id',
        'post_id'
    ];
    public function comment()
    {
        return $this->belongsTo('App\Models\Comment');
    }
    public function post()
    {
        return $this->belongsTo('App\Models\Post');
    }
}
