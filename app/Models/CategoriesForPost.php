<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoriesForPost extends Model
{
    //
    protected $fillable=[
        'category_id','post_id'
    ];
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }
    public function post()
    {
        return $this->belongsTo('App\Models\Post');
    }
}
