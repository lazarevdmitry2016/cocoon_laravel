<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    protected $fillable=[
        'name',
        'text',
        'date',
        'author_id'
    ];
    public function posts()
    {
        return $this->belongsToMany(
            'App\Models\Post',
            'comments_for_posts',
            'post_id',
            'comment_id'
        );
    }
}
