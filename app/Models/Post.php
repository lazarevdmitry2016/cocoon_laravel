<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Image;
use App\Tag;
use App\TagsForPost;
use Illuminate\Support\Facades\DB;


class Post extends Model
{
    //
    protected $fillable=[
        'href',
        'name',
        'date',
        'author_id',
        'image_id',
        'brief_content',
        'content',
        'description',
        'keywords'
    ];

    public function images()
    {
        return $this->belongsToMany(
            'App\Models\Image',
            'images_for_posts',
            'image_id',
            'post_id'
        );
    }

    public function image()
    {
        return $this->belongsTo('App\Models\Image');
    }

    public function comments()
    {
        return $this->belongsToMany(
            'App\Models\Comment',
            'comments_for_posts',
            'comment_id',
            'post_id'
        );

    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag','tags_for_posts','tag_id','post_id');
    }

    public function categories()
    {
        return $this->belongsToMany(
            'App\Models\Category',
            'categories_for_posts',
            'category_id',
            'post_id'
        );
    }
    public function authors()
    {
        return $this->belongsTo('App\Models\Author');
    }

    public function getDateFormattedAttribute()
    {
        $datetime=getdate(strtotime($this->date));
        $date=array(
            'day'=>$datetime['mday'],
            'month'=>$datetime['month'],
            'year'=>$datetime['year']
        );
        return $date;
    }

    // static methods

    /*
        Method makeResult()

        @param  $posts  array of posts

        @return     null
    */
    protected static function makeResult($posts)
    {
        $result=[];
        if (count($posts) > 0){
            foreach ($posts as $post)
            {
                $datetime=getdate(strtotime($post->date));
                $images=Image::find($post->image_id);

                $result[]=array(
                    'image'=>array(
                        'src'=>($images->count() > 0)?$images->first()->src:''
                    ),
                    'name'=>$post->name,
                    'date'=>array(
                        'day'=>$datetime['mday'],
                        'month'=>$datetime['month'],
                        'year'=>$datetime['year']
                    ),
                    'href'=>$post->permalink,
                    'author'=>array(
                        'name'=>Author::find($post->author_id)->first()->name
                    ),
                    'comments'=>array(),
                    'brief_content'=>$post->brief_content
                );
            }
        }
        return $result;

    }
}
