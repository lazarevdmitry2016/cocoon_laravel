<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
	//
	protected $fillable=[
		'created_at','updated_at','name','description'
	];

	public function images()
	{
		return $this->belongsToMany(
            'App\Models\Image',
            'images_for_services',
            'image_id',
            'service_id'
        );
	}


}
