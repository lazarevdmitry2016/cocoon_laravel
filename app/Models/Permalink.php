<?php

namespace App\Models;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Permalink extends Model
{
    //
    protected $fillable=[
        'created_at','updated_at','href','post_id'
    ];
    public function post()
    {
        return $this->hasOne('App\Models\Post');
    }

}
