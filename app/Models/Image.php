<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
  //
    protected $fillable=[
        'created_at','updated_at','src','alt'
    ];

    public function posts()
    {
        return $this->belongsToMany(
            'App\Models\Post',
            'images_for_posts',
            'post_id',
            'image_id'
        );
    }
    
    public function images_for_project()
    {
        return $this->hasMany('App\Models\ImagesForProject');
    }
    
    public function services()
    {
        return $this->belongsToMany(
            'App\Models\Service',
            'images_for_services',
            'service_id',
            'image_id'
        );
    }
    
    public function images_for_social()
    {
        return $this->hasMany('App\Models\ImagesForSocial');
    }

}
