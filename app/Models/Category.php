<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $fillable=[
        'name'
    ];

    public function posts()
    {
        return $this->belongsToMany(
            'App\Models\Post',
            'categories_for_posts',
            'post_id',
            'category_id'
        );
    }
}
