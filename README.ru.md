# Cocoon - Пример сайта-портфолио

Выберите ваш язык:


[English](README.md)

Русский


## Установка

### 1. Скопируйте репозиторий:

    git clone https://lazarevdmitry2016@bitbucket.org/lazarevdmitry2016/cocoon_laravel.git
    
    cd cocoon_laravel


### 2. Установите пакеты Composer'а:

    composer install


### 3. Установите данные Cocoon'а:

    php artisan cocoon:install


### 4. Протестируйте вашу установку:

    php artisan test

Если вы увидите одно предупреждение, то всё в порядке.


### 5. Запустите сервер для разработки:

    php artisan serve



## Об авторе

Дмитрий Лазарев. Email: [lazarevdmitry2008@gmail.com](mailto:lazarevdmitry2008@gmail.com)