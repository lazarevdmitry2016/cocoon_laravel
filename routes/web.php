<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Home;
use App\Http\Controllers\About;
use App\Http\Controllers\Blog;
use App\Http\Controllers\Contact;
use App\Http\Controllers\Portfolio;
use App\Http\Controllers\Project;
use App\Http\Controllers\Services;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Home::class, 'index'])->name('home');

Route::get('/about', [About::class, 'index'])->name('about');

Route::get('/blog',[Blog::class, 'index'])->name('blog'); // views all of the posts
Route::get('/blog/tag/{tag}',[Blog::class, 'getPostsByTag']);  // views all of the posts by tag
Route::get('/blog/category/{category_id}',[Blog::class, 'getPostsByCategoryId']);  // views all of the posts by category_id

Route::get('/posts/{slug}', [Blog::class, 'getPostBySlug']);
Route::get('/posts/id/{id}', [Blog::class, 'getPostById']);

Route::get('/contact',[Contact::class, 'index'])->name('contact');
Route::post('/contact',[Contact::class, 'createSubmission']);

Route::get('/portfolio',[Portfolio::class, 'index'])->name('portfolio');
Route::get('/portfolio/projects/{projectHref}', [Project::class, 'index']);

Route::get('/project/tag/{projectTag}',[Portfolio::class, 'getProjectsByTag']);
Route::get('/project/type/{projectType}',[Portfolio::class, 'getProjectsByType']);

Route::get('/services', [Services::class, 'index'])->name('services');
