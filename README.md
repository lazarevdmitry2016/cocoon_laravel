# Cocoon - Sample Portfolio Site

Choose your language:


English

[Русский](README.ru.md)


## Installation

### 1. Clone a repository:

    git clone https://lazarevdmitry2016@bitbucket.org/lazarevdmitry2016/cocoon_laravel.git

    cd cocoon_laravel


### 2. Install Composer packages:

    composer install


### 3. Install Cocoon seeds:

    php artisan cocoon:install


### 4. Test your installation:

    php artisan test

It's ok if you'll see one warning.


### 5. Start a development server:

    php artisan serve



## About Author

Dmitry Lazarev. Email: [lazarevdmitry2008@gmail.com](mailto:lazarevdmitry2008@gmail.com)