<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Models\Post;
use App\Models\Category;

class BlogTest extends TestCase
{

    public function testBlog()
    {
        $response = $this->get('/blog');

        $response->assertStatus(200);
    }

    public function testBlogTags()
    {
        $posts = Post::with('tags'); //->all();
        foreach ($posts as $post)
        {
            if ($post->tags->count() > 0)
            {
                $response = $this->get('/blog/tag'.($post->tags->first()->href));

                $response->assertStatus(200);
            }
            
        }

    }


}
