<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ServicesTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testServices()
    {
        $response = $this->get('/services');

        $response->assertStatus(200);
    }
}
