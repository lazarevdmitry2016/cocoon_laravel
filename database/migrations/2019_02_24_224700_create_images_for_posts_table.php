<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesForPostsTable extends Migration
{
    /*
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images_for_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('image_id')->unsigned();
            $table->foreign('image_id')->references('id')->on('images')->onDelete('cascade');

            $table->integer('post_id')->unsigned();
            $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');
        });
    }

    /*
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('images_for_posts',function(Blueprint $table){
            $table->dropForeign(['image_id']);
            $table->dropForeign(['post_id']);
        });
        Schema::dropIfExists('images_for_posts');
    }
}
