<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesForSocialsTable extends Migration
{
    /*
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images_for_socials', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('image_id')->unsigned();
            $table->foreign('image_id')->references('id')->on('images')->onDelete('cascade');

            $table->integer('social_id')->unsigned();
            $table->foreign('social_id')->references('id')->on('socials')->onDelete('cascade');
        });
    }

    /*
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('images_for_socials',function(Blueprint $table){
            $table->dropForeign(['image_id']);
            $table->dropForeign(['social_id']);
        });
        Schema::dropIfExists('images_for_socials');
    }
}
