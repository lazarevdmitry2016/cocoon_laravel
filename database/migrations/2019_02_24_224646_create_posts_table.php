<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /*
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('href');
            $table->string('name');
            $table->datetime('date');
            $table->integer('author_id')->unsigned();
            $table->foreign('author_id')->references('id')->on('authors')->onDelete('cascade');

            $table->integer('image_id')->unsigned();  // main image for the post
            $table->foreign('image_id')->references('id')->on('images')->onDelete('cascade');

            $table->longText('brief_content');
            $table->longText('content');
            $table->string('description');
            $table->string('keywords');
            $table->string('permalink')->nullable();
        });
    }

    /*
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts',function(Blueprint $table){
            $table->dropForeign(['image_id']);
            $table->dropForeign(['author_id']);
        });
        Schema::dropIfExists('posts');
    }
}
