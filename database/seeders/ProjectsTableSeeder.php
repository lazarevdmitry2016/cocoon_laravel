<?php

namespace Database\Seeders;use Illuminate\Database\Seeder;use Illuminate\Support\Facades\DB;
use App\Models\Project;

class ProjectsTableSeeder extends Seeder
{
    /*
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // DB::table('projects')->truncate();

        Project::create([
            'href'=>1,
            'name'=>'Project 0',
            'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ut leo tempus, pellentesque ante sit amet, lobortis tellus. Sed id nisi vel erat pellentesque euismod maximus vulputate libero. Proin id commodo mi. Donec pretium non orci eget ultricies. Curabitur ut mi eu quam sagittis facilisis a at est. Phasellus metus nisi, aliquet vitae magna a, ornare tincidunt nibh. Integer in tristique diam, sed ultricies quam. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla nec bibendum risus, nec semper urna.

Donec ultrices ex ut nibh aliquet, dapibus mattis metus laoreet. Duis finibus felis nec diam interdum rhoncus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec id varius nibh. Vestibulum a pretium massa. Etiam semper aliquam eros, pulvinar rhoncus massa aliquet et. Duis mollis viverra elit, vel vulputate orci blandit at.'
        ]);
        Project::create([
            'href'=>2,
            'name'=>'Project 1',
            'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ut leo tempus, pellentesque ante sit amet, lobortis tellus. Sed id nisi vel erat pellentesque euismod maximus vulputate libero. Proin id commodo mi. Donec pretium non orci eget ultricies. Curabitur ut mi eu quam sagittis facilisis a at est. Phasellus metus nisi, aliquet vitae magna a, ornare tincidunt nibh. Integer in tristique diam, sed ultricies quam. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla nec bibendum risus, nec semper urna.

Donec ultrices ex ut nibh aliquet, dapibus mattis metus laoreet. Duis finibus felis nec diam interdum rhoncus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec id varius nibh. Vestibulum a pretium massa. Etiam semper aliquam eros, pulvinar rhoncus massa aliquet et. Duis mollis viverra elit, vel vulputate orci blandit at.'
        ]);
        Project::create([
            'href'=>3,
            'name'=>'Project 2',
            'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ut leo tempus, pellentesque ante sit amet, lobortis tellus. Sed id nisi vel erat pellentesque euismod maximus vulputate libero. Proin id commodo mi. Donec pretium non orci eget ultricies. Curabitur ut mi eu quam sagittis facilisis a at est. Phasellus metus nisi, aliquet vitae magna a, ornare tincidunt nibh. Integer in tristique diam, sed ultricies quam. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla nec bibendum risus, nec semper urna.

Donec ultrices ex ut nibh aliquet, dapibus mattis metus laoreet. Duis finibus felis nec diam interdum rhoncus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec id varius nibh. Vestibulum a pretium massa. Etiam semper aliquam eros, pulvinar rhoncus massa aliquet et. Duis mollis viverra elit, vel vulputate orci blandit at.'
        ]);
    }
}
