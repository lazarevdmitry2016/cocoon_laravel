<?php

namespace Database\Seeders;use Illuminate\Database\Seeder;use Illuminate\Support\Facades\DB;
use App\Models\Tag;

class TagsTableSeeder extends Seeder
{
    /*
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // DB::table('tags')->truncate();

        Tag::create([
            'href'=>'test1',
            'name'=>'test1'
        ]);
        Tag::create([
            'href'=>'test2',
            'name'=>'test2'
        ]);

    }
}
