<?php

namespace Database\Seeders;use Illuminate\Database\Seeder;use Illuminate\Support\Facades\DB;
use App\Models\ImagesForProject;
use App\Models\Image;
use App\Models\Project;

class ImagesForProjectsTableSeeder extends Seeder
{
    /*
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // DB::table('images_for_projects')->truncate();
        ImagesForProject::create([
            'image_id'=>Image::find(1)->id,
            'project_id'=>Project::find(1)->id
        ]);
        ImagesForProject::create([
            'image_id'=>Image::find(2)->id,
            'project_id'=>Project::find(2)->id
        ]);
        ImagesForProject::create([
            'image_id'=>Image::find(3)->id,
            'project_id'=>Project::find(3)->id
        ]);
    }
}
