<?php

namespace Database\Seeders;use Illuminate\Database\Seeder;use Illuminate\Support\Facades\DB;
use App\Models\CommentsForPost;
use App\Models\Comment;
use App\Models\Post;

class CommentsForPostsTableSeeder extends Seeder
{
    /*
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // DB::table('comments_for_posts')->truncate();

        CommentsForPost::create([
            'comment_id'=>Comment::find(1)->id,
            'post_id'=>Post::find(1)->id
        ]);
    }
}
