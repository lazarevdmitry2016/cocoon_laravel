<?php

namespace Database\Seeders;use Illuminate\Database\Seeder;use Illuminate\Support\Facades\DB;
use App\Models\Social;

class SocialsTableSeeder extends Seeder
{
    /*
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // DB::table('socials')->truncate();

        Social::create([
            'href'=>'',
            'name'=>'instagram'
        ]);
    }
}
