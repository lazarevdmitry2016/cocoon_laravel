<?php

namespace Database\Seeders;use Illuminate\Database\Seeder;
use App\Models\TagsForPost;
use App\Models\Tag;
use App\Models\Post;

class TagsForPostsTableSeeder extends Seeder
{
    /*
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // DB::table('tags_for_posts')->truncate();

        TagsForPost::create([
            'tag_id'=>Tag::find(1)->id,
            'post_id'=>Post::find(1)->id
        ]);
        TagsForPost::create([
            'tag_id'=>Tag::find(2)->id,
            'post_id'=>Post::find(2)->id
        ]);
        TagsForPost::create([
            'tag_id'=>Tag::find(1)->id,
            'post_id'=>Post::find(3)->id
        ]);
    }
}
