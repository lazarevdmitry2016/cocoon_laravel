<?php

namespace Database\Seeders;use Illuminate\Database\Seeder;use Illuminate\Support\Facades\DB;
use App\Models\Information;
class InformationTableSeeder extends Seeder
{
    /*
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // DB::table('information')->truncate();

        Information::create([
            'key'=>'about_me_title',
            'value'=>'About Me Title'
        ]);
        Information::create([
            'key'=>'about_me_text',
            'value'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ut leo tempus, pellentesque ante sit amet, lobortis tellus. Sed id nisi vel erat pellentesque euismod maximus vulputate libero. Proin id commodo mi. Donec pretium non orci eget ultricies. Curabitur ut mi eu quam sagittis facilisis a at est. Phasellus metus nisi, aliquet vitae magna a, ornare tincidunt nibh. Integer in tristique diam, sed ultricies quam. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla nec bibendum risus, nec semper urna.'
        ]);
        Information::create([
            'key'=>'contact_title',
            'value'=>'Contact Title'
        ]);
        Information::create([
            'key'=>'contact_text',
            'value'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ut leo tempus, pellentesque ante sit amet, lobortis tellus. Sed id nisi vel erat pellentesque euismod maximus vulputate libero. Proin id commodo mi. Donec pretium non orci eget ultricies. Curabitur ut mi eu quam sagittis facilisis a at est. Phasellus metus nisi, aliquet vitae magna a, ornare tincidunt nibh. Integer in tristique diam, sed ultricies quam. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla nec bibendum risus, nec semper urna.'
        ]);
        Information::create([
            'key'=>'contact_email',
            'value'=>'admin@sample.com'
        ]);
        Information::create([
            'key'=>'contact_phonenumber',
            'value'=>'+7(123)456-78-90'
        ]);
        Information::create([
            'key'=>'contact_address',
            'value'=>'Contact Address'
        ]);
        // home page
        Information::create([
            'key'=>'home_description',
            'value'=>'home page description'
        ]);
        Information::create([
            'key'=>'home_keywords',
            'value'=>'home page keywords'
        ]);
        // about page
        Information::create([
            'key'=>'about_description',
            'value'=>'about page description'
        ]);
        Information::create([
            'key'=>'about_keywords',
            'value'=>'about page keywords'
        ]);
        // contact page
        Information::create([
            'key'=>'contact_description',
            'value'=>'contact page description'
        ]);
        Information::create([
            'key'=>'contact_keywords',
            'value'=>'contact page keywords'
        ]);
        // portfolio
        Information::create([
            'key'=>'portfolio_description',
            'value'=>'portfolio page description'
        ]);
        Information::create([
            'key'=>'portfolio_keywords',
            'value'=>'portfolio page keywords'
        ]);
        // project page
        Information::create([
            'key'=>'project_description',
            'value'=>'project page description'
        ]);
        Information::create([
            'key'=>'project_keywords',
            'value'=>'project page keywords'
        ]);
        // services page
        Information::create([
            'key'=>'services_description',
            'value'=>'services page description'
        ]);
        Information::create([
            'key'=>'services_keywords',
            'value'=>'services page keywords'
        ]);
        // author
        Information::create([
            'key'=>'author',
            'value'=>'Dmitry Lazarev'
        ]);

    }
}
