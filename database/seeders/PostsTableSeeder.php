<?php

namespace Database\Seeders;use Illuminate\Database\Seeder;use Illuminate\Support\Facades\DB;
use App\Models\Post;
use App\Models\Image;
use App\Models\Author;
use App\Models\Permalink;

class PostsTableSeeder extends Seeder
{
    /*
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // DB::table('posts')->truncate();
        $date = '2019-01-01 03:00:00';
        $brief_content = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ut leo tempus, pellentesque ante sit amet, lobortis tellus.';
        $content = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ut leo tempus, pellentesque ante sit amet, lobortis tellus. Sed id nisi vel erat pellentesque euismod maximus vulputate libero. Proin id commodo mi. Donec pretium non orci eget ultricies. Curabitur ut mi eu quam sagittis facilisis a at est. Phasellus metus nisi, aliquet vitae magna a, ornare tincidunt nibh. Integer in tristique diam, sed ultricies quam. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla nec bibendum risus, nec semper urna.

Donec ultrices ex ut nibh aliquet, dapibus mattis metus laoreet. Duis finibus felis nec diam interdum rhoncus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec id varius nibh. Vestibulum a pretium massa. Etiam semper aliquam eros, pulvinar rhoncus massa aliquet et. Duis mollis viverra elit, vel vulputate orci blandit at.

Cras laoreet tempor consectetur. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum orci tortor, tempor in auctor eu, sodales sit amet purus. Nullam consequat lobortis molestie. In vestibulum ut sapien sed lacinia. Nam ac nisl vitae lacus tempor feugiat nec nec dui. Nunc nec felis ante. Etiam quis elit vulputate, bibendum felis consectetur, lobortis urna. Etiam rutrum lobortis magna, lobortis egestas risus viverra eu. Nulla rutrum tincidunt iaculis. Sed metus neque, ornare vel dapibus nec, dapibus sed nisi.

Nullam euismod diam eleifend ante maximus feugiat. Nulla sit amet sem sit amet dolor suscipit ultrices. Morbi ac mauris at lorem venenatis tempus. Nullam viverra dignissim sem. Etiam convallis turpis nunc, et porta sapien dictum eget. Donec id ex nisl. Nam dui augue, vehicula pharetra diam ac, blandit rutrum justo. Nunc vel vestibulum ligula, a faucibus enim. Suspendisse ac facilisis quam. Mauris dolor quam, aliquam eu rhoncus non, pellentesque a nibh. Donec sed tortor nunc. Vivamus eleifend sem ultrices porta tincidunt.

Praesent egestas leo nec nunc auctor suscipit. Sed id lorem non libero lacinia cursus eget eu nibh. Sed convallis, massa molestie ultrices elementum, dolor mauris tincidunt ligula, in suscipit sapien tortor et diam. Curabitur sed ultricies arcu, sagittis vehicula risus. Pellentesque viverra gravida diam ac imperdiet. Etiam dictum dui ipsum, ultricies efficitur tellus blandit vitae. Pellentesque dignissim lacus vel diam tincidunt, eget suscipit neque posuere. Donec id leo rutrum, rhoncus ante a, maximus tellus. Donec molestie congue tellus, in mattis ipsum aliquam ac. Etiam id risus elit. Pellentesque neque leo, molestie at lectus sed, venenatis varius diam. Quisque vitae gravida nisi, non mollis libero. Donec pretium hendrerit mi, aliquet hendrerit velit feugiat quis. Maecenas non accumsan augue, gravida porta arcu. Duis quis risus a eros sagittis malesuada eget lacinia arcu.';
        $author = Author::create([
          'name'=>'Admin'
        ]);;
        for ($i = 1; $i < 4; $i++)
        {
            $image = Image::create([
                'src' => "/assets/img/blog/blog$i.png",
                'alt' => "post #$i"
            ]);
            
            Post::create([
                'href'=>"$i",
                'name'=>"post$i",
                'date'=> $date,
                'description'=>"Post $i",
                'keywords'=>"post $i keywords",
                'brief_content'=> $brief_content,
                'content'=> $content,
                'image_id'=>$image->id,
                'author_id'=> $author->id,
                'permalink'=>"post$i"
            ]);
        
        }
        
    }
}
