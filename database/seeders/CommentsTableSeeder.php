<?php

namespace Database\Seeders;use Illuminate\Database\Seeder;use Illuminate\Support\Facades\DB;
use App\Models\Comment;
use App\Models\Author;

class CommentsTableSeeder extends Seeder
{
    /*
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // DB::table('comments')->truncate();

        Comment::create([
            'text'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ut leo tempus, pellentesque ante sit amet, lobortis tellus. Sed id nisi vel erat pellentesque euismod maximus vulputate libero. Proin id commodo mi. Donec pretium non orci eget ultricies. Curabitur ut mi eu quam sagittis facilisis a at est. Phasellus metus nisi, aliquet vitae magna a, ornare tincidunt nibh. Integer in tristique diam, sed ultricies quam. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla nec bibendum risus, nec semper urna.',
            'date'=>'2019-01-01 01:02:03',
            'author_id'=>Author::find(1)->id
        ]);
    }
}
