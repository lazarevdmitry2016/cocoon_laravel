<?php

namespace Database\Seeders;use Illuminate\Database\Seeder;
use App\Models\Image;

class ImagesTableSeeder extends Seeder
{
    /*
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // DB::table('images')->truncate();


        $this->insertImage(['logo.png','logo']);

        $this->insertImage(['blog/instagram/inst1.png','instagram1']);
        $this->insertImage(['blog/instagram/inst2.png','instagram2']);
        $this->insertImage(['blog/instagram/inst3.png','instagram3']);
        $this->insertImage(['blog/instagram/inst4.png','instagram4']);
        $this->insertImage(['blog/instagram/inst5.png','instagram5']);
        $this->insertImage(['blog/instagram/inst6.png','instagram6']);

        $this->insertImage(['portfolio/port1.png','portfolio']);
        $this->insertImage(['portfolio/port2.png','portfolio']);
        $this->insertImage(['portfolio/port3.png','portfolio']);
        $this->insertImage(['portfolio/port4.png','portfolio']);
        $this->insertImage(['portfolio/port5.png','portfolio']);
        $this->insertImage(['portfolio/port6.png','portfolio']);
        $this->insertImage(['portfolio/port7.png','portfolio']);
        $this->insertImage(['portfolio/port8.png','portfolio']);
        $this->insertImage(['portfolio/port9.png','portfolio']);
        $this->insertImage(['portfolio/port10.png','portfolio']);
        $this->insertImage(['portfolio/port11.png','portfolio']);

    }
    protected function insertImage($image){
        Image::create([
            'src'=>$image[0],
            'alt'=>$image[1]
        ]);
    }
}
