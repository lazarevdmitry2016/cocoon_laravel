<?php

namespace Database\Seeders;use Illuminate\Database\Seeder;use Illuminate\Support\Facades\DB;
use App\Models\Category;

class CategoriesTableSeeder extends Seeder
{
    /*
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // DB::table('categories')->truncate();
        Category::create([
            'name'=>'Category1'
        ]);
        Category::create([
            'name'=>'Category2'
        ]);
    }
}
