<?php

namespace Database\Seeders;use Illuminate\Database\Seeder;use Illuminate\Support\Facades\DB;
use App\Models\Author;

class AuthorsTableSeeder extends Seeder
{
    /*
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('authors')->truncate();
        //

        Author::create([
          'name'=>'Editor'
        ]);
        Author::create([
          'name'=>'Moderator'
        ]);
    }
}
