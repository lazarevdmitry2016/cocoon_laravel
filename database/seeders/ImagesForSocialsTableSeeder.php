<?php

namespace Database\Seeders;use Illuminate\Database\Seeder;use Illuminate\Support\Facades\DB;
use App\Models\ImagesForSocial;
use App\Models\Image;
use App\Models\Social;

class ImagesForSocialsTableSeeder extends Seeder
{
    /*
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // DB::table('images_for_socials')->truncate();
        for ($i=5;$i<11;$i++){
            ImagesForSocial::create([
                'social_id'=>Social::find(1)->id,
                'image_id'=>Image::find(1)->id
            ]);
        }

    }
}
