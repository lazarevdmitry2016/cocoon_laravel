<?php

namespace Database\Seeders;use Illuminate\Database\Seeder;use Illuminate\Support\Facades\DB;
use App\Models\CategoriesForPost;
use App\Models\Post;
use App\Models\Category;

class CategoriesForPostsTableSeeder extends Seeder
{
    /*
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // DB::table('categories_for_posts')->truncate();

        CategoriesForPost::create([
            'category_id'=>Category::find(1)->id,
            'post_id'=>Post::find(1)->id
        ]);
        CategoriesForPost::create([
            'category_id'=>Category::find(2)->id,
            'post_id'=>Post::find(2)->id
        ]);
        CategoriesForPost::create([
            'category_id'=>Category::find(2)->id,
            'post_id'=>Post::find(2)->id
        ]);
    }
}
