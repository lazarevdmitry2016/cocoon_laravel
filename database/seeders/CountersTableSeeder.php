<?php

namespace Database\Seeders;use Illuminate\Database\Seeder;
use App\Models\Counter;

class CountersTableSeeder extends Seeder
{
    /*
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // DB::table('counters')->truncate();

        Counter::create([
            'number'=>12,
            'text'=>'Years of experience'
        ]);
        Counter::create([
            'number'=>257,
            'text'=>'Happy Clients'
        ]);
        Counter::create([
            'number'=>192,
            'text'=>'Projects Completed'
        ]);
    }
}
