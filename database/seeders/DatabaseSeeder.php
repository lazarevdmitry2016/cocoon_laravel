<?php

namespace Database\Seeders;

namespace Database\Seeders;use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            InformationTableSeeder::class,
            AuthorsTableSeeder::class,
            CategoriesTableSeeder::class,
            CommentsTableSeeder::class,
            ImagesTableSeeder::class,
            PostsTableSeeder::class,
            ProjectsTableSeeder::class,
            ServicesTableSeeder::class,
            SocialsTableSeeder::class,
            TagsTableSeeder::class,
            CountersTableSeeder::class,
            TestimonialsTableSeeder::class,

            CategoriesForPostsTableSeeder::class,
            CommentsForPostsTableSeeder::class,
            ImagesForPostsTableSeeder::class,
            ImagesForProjectsTableSeeder::class,
            ImagesForServicesTableSeeder::class,
            ImagesForSocialsTableSeeder::class,
            TagsForPostsTableSeeder::class,
            PermalinksTableSeeder::class
        ]);
    }
}
